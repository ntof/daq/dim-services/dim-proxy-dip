/*
 * DIPClient.cpp
 *
 *  Created on: Feb 5, 2015
 *      Author: mdonze
 */
#include "DIPClient.h"

#include <string>
#include <thread>

#include <boost/interprocess/ipc/message_queue.hpp>

#include "Constants.h"
#include "DIPSource.h"

#include <dic.hxx>

namespace ntof {
namespace proxy {

DIPClient::DIPClient() :
    sourceName("DIP"),
    maxMsg(MAX_MSG),
    maxMsgSize(MAX_MSG_SIZE),
    queueName(MQ_NAME)
{}

DIPClient::~DIPClient()
{
    mq.reset();
}

void DIPClient::postData(pugi::xml_document &doc)
{
    std::ostringstream oss;
    // Uncomment end of line for raw XML output
    doc.save(oss); //,"\t", pugi::format_raw);
    try
    {
        mq->try_send(oss.str().c_str(), oss.str().size(), 0);
    }
    catch (boost::interprocess::interprocess_exception &ex)
    {
        std::cerr << "Failed to send message: " << ex.what() << std::endl;
    }
}

bool DIPClient::loadConfig(std::string cfgPath)
{
    pugi::xml_document doc;
    pugi::xml_parse_result res = doc.load_file(cfgPath.c_str());
    if (res)
    {
        std::string dnsNode = "dipns1.cern.ch,dipns2.cern.ch";
        pugi::xml_node root = doc.root().child("proxy");
        pugi::xml_node cfg = root.child("configuration");
        if (cfg)
        {
            if (cfg.child("dipDNSNode").attribute("name"))
            {
                dnsNode = cfg.child("dipDNSNode")
                              .attribute("name")
                              .as_string(dnsNode.c_str());
            }
            if (cfg.child("sourceName").attribute("name"))
            {
                sourceName = cfg.child("sourceName")
                                 .attribute("name")
                                 .as_string(sourceName.c_str());
            }

            pugi::xml_node queueCfg = cfg.child("queue");
            if (queueCfg)
            {
                queueName = queueCfg.attribute("name").as_string(MQ_NAME);
                maxMsg = queueCfg.attribute("count").as_int(MAX_MSG);
                maxMsgSize = queueCfg.attribute("size").as_int(MAX_MSG_SIZE);
            }
        }
        DIPSource::setDIPDNSNode(dnsNode.c_str());

        mq.reset();
        while (!mq)
        {
            try
            {
                mq.reset(new boost::interprocess::message_queue(
                    boost::interprocess::open_only, queueName.c_str()));
            }
            catch (...)
            {
                std::cerr << "Waiting for message-queue" << std::endl;
                std::this_thread::sleep_for(std::chrono::seconds(3));
            }
        }

        pugi::xml_node sourcesNode = root.child("sources");
        if (sourcesNode)
        {
            for (pugi::xml_node source = sourcesNode.first_child(); source;
                 source = source.next_sibling())
            {
                if (source.attribute("service") &&
                    source.attribute("destination"))
                {
                    sources.push_back(new ntof::proxy::DIPSource(
                        source.attribute("service").as_string(""),
                        source.attribute("destination").as_string(""), this));
                }
                else
                {
                    std::cerr << "Bad DIP service description." << std::endl;
                    std::cerr << "Expected service and destination attributes"
                              << std::endl;
                }
            }
        }

        return true;
    }
    else
    {
        return false;
    }
}

void DIPClient::heartBeat()
{
    while (true)
    {
        std::this_thread::sleep_for(std::chrono::seconds(HEARTBEAT_RATE));
        pugi::xml_document hbDoc;
        pugi::xml_node hbNode = hbDoc.append_child("heartbeat");
        hbNode.append_attribute("source").set_value(sourceName.c_str());
        postData(hbDoc);
    }
}

/**
 * Gets the message queue configuration
 * @param name
 * @param msgMax
 * @param msgMaxSize
 */
void DIPClient::getQueueConfiguration(std::string &name,
                                      std::size_t &msgMax,
                                      std::size_t &msgMaxSize)
{
    name = queueName;
    msgMax = maxMsg;
    msgMaxSize = maxMsgSize;
}

} /* namespace proxy */
} /* namespace ntof */
