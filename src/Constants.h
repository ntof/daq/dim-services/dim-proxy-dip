/*
 * Constants.h
 *
 *  Created on: Feb 6, 2015
 *      Author: mdonze
 */

#ifndef CONSTANTS_H_
#define CONSTANTS_H_

#define CFG_NAME "/etc/ntof/proxy-dip.xml"
#define HEARTBEAT_RATE 5
#define MAX_MSG_SIZE 65535
#define MAX_MSG 2000
#define MQ_NAME "proxyMQ"

#endif /* CONSTANTS_H_ */
