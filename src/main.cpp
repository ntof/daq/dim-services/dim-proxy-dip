/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-06-17T22:16:42+02:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#include <iostream>

#include "Constants.h"
#include "DIPClient.h"

int main(int argc, char **argv)
{
    ntof::proxy::DIPClient client;
    std::string cfgName = CFG_NAME;
    if (argc > 1)
    {
        cfgName = argv[1];
    }
    if (!client.loadConfig(cfgName))
    {
        std::cerr << "FATAL : Read config failed..." << std::endl;
        return 1;
    }
    while (1)
    {
        client.heartBeat();
    }
    return 0;
}
