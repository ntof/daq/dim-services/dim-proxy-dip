/*
 * Utils.h
 *
 *  Created on: Jan 15, 2015
 *      Author: mdonze
 */

#ifndef UTILS_H_
#define UTILS_H_
#include <stdint.h>

namespace ntof {
namespace proxy {

class Utils
{
public:
    static int64_t getTimeMs();
};

} /* namespace proxy */
} /* namespace ntof */

#endif /* UTILS_H_ */
