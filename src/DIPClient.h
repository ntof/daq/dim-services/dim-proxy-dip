/*
 * DIPClient.h
 *
 *  Created on: Feb 5, 2015
 *      Author: mdonze
 */

#ifndef DIPCLIENT_H_
#define DIPCLIENT_H_

#include <memory>
#include <vector>

#include <boost/interprocess/ipc/message_queue.hpp>

#include <pugixml.hpp>

namespace ntof {
namespace proxy {
class DIPSource;

class DIPClient
{
public:
    DIPClient();
    virtual ~DIPClient();
    void postData(pugi::xml_document &doc);
    bool loadConfig(std::string cfgPath);
    void heartBeat(); //!<< Heart beat watchdog

    inline const std::string &getSourceName() const { return sourceName; }
    /**
     * Gets the message queue configuration
     * @param name
     * @param msgMax
     * @param msgMaxSize
     */
    void getQueueConfiguration(std::string &name,
                               std::size_t &msgMax,
                               std::size_t &msgMaxSize);

private:
    std::string sourceName;
    std::size_t maxMsg;
    std::size_t maxMsgSize;
    std::string queueName;

    std::unique_ptr<boost::interprocess::message_queue> mq;
    std::vector<ntof::proxy::DIPSource *> sources;
};

} /* namespace proxy */
} /* namespace ntof */

#endif /* DIPCLIENT_H_ */
