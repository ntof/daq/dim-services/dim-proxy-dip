/*
 * DIPSource.h
 *  DIP data source
 *  Created on: Jan 15, 2015
 *      Author: mdonze
 */

#ifndef DIPSOURCE_H_
#define DIPSOURCE_H_

#include <string>

#include <boost/thread.hpp>

#include <pugixml.hpp>

#include "Dip.h"
#include "DipSubscription.h"

namespace ntof {
namespace proxy {
class DIPClient;

class DIPSource : public DipSubscriptionListener
{
public:
    DIPSource(const std::string &dimSrc,
              const std::string &dimDest,
              DIPClient *client);
    virtual ~DIPSource();

    /**
     * Handler for DIP callback
     * @param subscription
     * @param message
     */
    void handleMessage(DipSubscription *subscription, DipData &message);
    void disconnected(DipSubscription *subscription, char *reason);
    void connected(DipSubscription *subscription);
    void handleException(DipSubscription *subscription, DipException &ex);

    /**
     * Sets DIP DNS node
     * @param nodes
     */
    static void setDIPDNSNode(const std::string &nodes);

private:
    std::string dipSvcName;  //!<< DIP service name
    std::string destName;    //!<< Destination name
    DipSubscription *dipSub; //!<< DIP subscription object
    DIPClient *client_;      //!<< Reference to client
    pugi::xml_document doc;  //!<< This XML document
    static DipFactory *dip;
    static std::string dipDNSNode; //!<< DIP DNS node(s)

    void connect();                               //!<< Connect to DIP
    DIPSource(const DIPSource &other);            //!<< Don't allow copy
    DIPSource &operator=(const DIPSource &other); //!<< Don't allow copy
};

} /* namespace proxy */
} /* namespace ntof */

#endif /* DIPSOURCE_H_ */
