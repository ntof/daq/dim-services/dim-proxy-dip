/*
 * Utils.cpp
 *
 *  Created on: Jan 15, 2015
 *      Author: mdonze
 */

#include "Utils.h"

#include <sys/time.h>

namespace ntof {
namespace proxy {

int64_t Utils::getTimeMs()
{
    struct timeval tv;
    gettimeofday(&tv, 0);
    return (tv.tv_sec * 1000) + (tv.tv_usec / 1000);
}

} /* namespace proxy */
} /* namespace ntof */
