/*
 * DIPSource.cpp
 *
 *  Created on: Feb 5, 2015
 *      Author: mdonze
 */

#include "DIPSource.h"

#include <iostream>

#include "Constants.h"
#include "DIMData.h"
#include "DIPClient.h"
#include "Utils.h"
#include "pugixml.hpp"

namespace ntof {
namespace proxy {

DipFactory *DIPSource::dip = NULL;
std::string DIPSource::dipDNSNode;

DIPSource::DIPSource(const std::string &dipSrc,
                     const std::string &dimDest,
                     DIPClient *client) :
    dipSvcName(dipSrc), destName(dimDest), client_(client)
{
    pugi::xml_node aqnNode = doc.append_child("acquisition");
    aqnNode.append_attribute("source").set_value(
        client_->getSourceName().c_str());
    aqnNode.append_attribute("from").set_value(dipSrc.c_str());
    aqnNode.append_attribute("to").set_value(destName.c_str());
    client_->postData(doc);
    connect();
}

DIPSource::~DIPSource()
{
    if (dip != NULL)
    {
        if (dipSub != NULL)
        {
            dip->destroyDipSubscription(dipSub);
        }
    }
}
void DIPSource::handleMessage(DipSubscription *subscription, DipData &message)
{
    int size = 0;
    const char **tags = message.getTags(size);

    doc.reset();
    pugi::xml_node aqnNode = doc.append_child("acquisition");
    aqnNode.append_attribute("source").set_value(
        client_->getSourceName().c_str());
    aqnNode.append_attribute("from").set_value(subscription->getTopicName());
    aqnNode.append_attribute("to").set_value(destName.c_str());
    pugi::xml_node dataNode = aqnNode.append_child("dataset");
    for (int i = 0; i < size; ++i)
    {
        ntof::dim::DIMData *data = NULL;
        switch (message.getValueType(tags[i]))
        {
        case TYPE_BYTE:
            data = new ntof::dim::DIMData(i, tags[i], "",
                                          message.extractByte(tags[i]));
            break;
        case TYPE_SHORT:
            data = new ntof::dim::DIMData(i, tags[i], "",
                                          message.extractShort(tags[i]));
            break;
        case TYPE_INT:
            data = new ntof::dim::DIMData(i, tags[i], "",
                                          message.extractInt(tags[i]));
            break;
        case TYPE_LONG:
            data = new ntof::dim::DIMData(i, tags[i], "",
                                          message.extractLong(tags[i]));
            break;
        case TYPE_FLOAT:
            data = new ntof::dim::DIMData(i, tags[i], "",
                                          message.extractFloat(tags[i]));
            break;
        case TYPE_DOUBLE:
            data = new ntof::dim::DIMData(i, tags[i], "",
                                          message.extractDouble(tags[i]));
            break;
        case TYPE_STRING:
            data = new ntof::dim::DIMData(i, tags[i], "",
                                          message.extractString(tags[i]));
            break;
        case TYPE_BOOLEAN:
            data = new ntof::dim::DIMData(i, tags[i], "",
                                          message.extractBool(tags[i]));
            break;
        default:
            std::cerr << "DIP source " << dipSvcName
                      << " unsupported datatype for " << tags[i] << std::endl;
            break;
        }
        if (data)
        {
            data->insertIntoAqn(dataNode);
            delete data;
        }
    }
    client_->postData(doc);
}

void DIPSource::disconnected(DipSubscription *subscription, char *reason)
{
    doc.reset();
    pugi::xml_node aqnNode = doc.append_child("remove");
    aqnNode.append_attribute("source").set_value(
        client_->getSourceName().c_str());
    aqnNode.append_attribute("from").set_value(subscription->getTopicName());
    aqnNode.append_attribute("to").set_value(destName.c_str());
    pugi::xml_node errNode = aqnNode.append_child("error");
    errNode.append_attribute("status").set_value("DISCONNECTED");
    errNode.append_child(pugi::node_pcdata).set_value(reason);
    client_->postData(doc);
}

void DIPSource::connected(DipSubscription * /*subscription*/)
{
    /*    doc.reset();
        pugi::xml_node aqnNode = doc.append_child("acquisition");
        aqnNode.append_attribute("source").set_value(client_->getSourceName().c_str());
        aqnNode.append_attribute("from").set_value(subscription->getTopicName());
        aqnNode.append_attribute("to").set_value(destName.c_str());
        aqnNode.append_attribute("updated").set_value((long long
       int)Utils::getTimeMs());
        aqnNode.append_attribute("status").set_value("CONNECTED");
        client_->postData(doc);
    */
}

void DIPSource::handleException(DipSubscription *subscription, DipException &ex)
{
    doc.reset();
    pugi::xml_node aqnNode = doc.append_child("acquisition");
    aqnNode.append_attribute("source").set_value(
        client_->getSourceName().c_str());
    aqnNode.append_attribute("from").set_value(subscription->getTopicName());
    aqnNode.append_attribute("to").set_value(destName.c_str());
    pugi::xml_node errNode = aqnNode.append_child("error");
    errNode.append_child(pugi::node_pcdata).set_value("NO LINK");
    errNode.append_attribute("status").set_value("ERROR");
    errNode.append_child(pugi::node_pcdata).set_value(ex.what());
    client_->postData(doc);
}

void DIPSource::connect()
{
    // Connect to DIP service
    if (dip == NULL)
    {
        dip = Dip::create("NTOF-proxy");
        if (!dipDNSNode.empty())
        {
            std::cout << "Setting DIP DNS node to " << dipDNSNode << std::endl;
            dip->setDNSNode(dipDNSNode.c_str());
        }
    }
    std::cout << "Creating DIP subscription to " << dipSvcName << std::endl;
    dipSub = dip->createDipSubscription(dipSvcName.c_str(), this);
}

/**
 * Sets DIP DNS node
 * @param nodes
 */
void DIPSource::setDIPDNSNode(const std::string &nodes)
{
    dipDNSNode = nodes;
}

} /* namespace proxy */
} /* namespace ntof */
