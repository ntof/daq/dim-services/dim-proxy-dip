# DIP proxy for DIM

DIP proxy for DIM

## Build

To build this project it is recommended to install [docker-builder](https://gitlab.cern.ch/apc/common/tools/x-builder) script.

```bash
docker-builder

rm -rf build && mkdir build
cd build && cmake3 .. -DTESTS=ON

make -j4
```

## Testing

<!--
To run unit-tests:
```bash
docker-builder ./build/tests/test_all
```
-->

To run linters:
```bash
docker-builder make -C build lint
```

## Running

To run this service on a local DIM/DNS node, edit `tests/data/sysmon.xml` to set the DIM/DNS address.

Then to connect the service from a container to this DNS:
```bash
docker-builder
export DIM_HOST_NODE=$(hostname -i)

./build/src/dim-proxy-dip -c ./tests/data/proxy-dip.xml -m ./tests/data/misc.xml
```

## Debugging

To compile this component for gdb/lldb:
```bash
docker-builder

cd build && cmake3 -DCMAKE_BUILD_TYPE=Debug ..
```
