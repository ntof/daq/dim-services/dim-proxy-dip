
set(CQUERY_CONFIG "${CMAKE_SOURCE_DIR}/.cquery")

file(WRITE "${CQUERY_CONFIG}" "")
file(APPEND "${CQUERY_CONFIG}" "%clang\n%cpp -std=gnu++11\n")
file(APPEND "${CQUERY_CONFIG}" "-Isrc\n-Iinclude\n")
file(APPEND "${CQUERY_CONFIG}" "-I/usr/include\n-I/usr/local/include\n")
file(APPEND "${CQUERY_CONFIG}" "-I${CMAKE_BINARY_DIR}\n-I${CMAKE_BINARY_DIR}/src\n")

foreach(DIR ${INCDIRS})
    file(APPEND "${CQUERY_CONFIG}" "-I${DIR}\n")
endforeach(DIR)

file(APPEND "${CQUERY_CONFIG}" "-Wno-everything\n")
